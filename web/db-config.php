<?php
// change these valuse according to yout environment
const DB_HOST = "localhost";
const DB_NAME = "blog";
const DB_USER = "root";
const DB_PASS = "root";

// don't change this one
const DB_DSN = 'mysql:host='.DB_HOST.';dbname='.DB_NAME;



$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", // encodage utf-8
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // gérer les erreurs en tant qu'exception
    PDO::ATTR_EMULATE_PREPARES => false // faire des vrais requêtes préparées et non une émulation
);