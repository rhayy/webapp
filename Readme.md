# Une super application web

## Structure de l'application

```

├── app
│   ├── web
│       ├── add.php
│       ├── db-config.php
│       ├── index.php
│       └── validation.php
│   ├── db
│   └── table.sql
└── Readme.md
```

L'application est composé des éléments suivants :
- `app/web` : répertoire contenant les fichiers php du front-end
- `app/db` : répertoire contenant des scripts sql permettant d'initialiser la base de données
- `Readme.md` : cette documentation

## Déploiement

### Front-end

Le déploiement du front-end nécessite l'installation d'un serveur http, comme apache ou nginx/php-fpm.

#### Apache

- installer apache, php, php-mysql.
- modifier le contenu de `app/web/db-config.php` et adapter les valeurs à votre environnement.
- copier le contenu de `app/web` dans `var/www/html`.